## What is photocrunch?

Photocrunch is a command-line tool that takes an input .psd (Photoshop) file and outputs layers as individual .png files, named after the layer names. The tool also outputs an info.xml that contains information on the PSD itself as well as each layer, including their absolute original position and size in the document.

Layers are trimmed to the smallest rectangle that contains pixels before being saved as a psd.

Photocrunch also contains, to my knowledge, the best opensource .psd reader library there is. It is much more capable than the importer found in the Gimp for instance and reads more modern files. If you are so inclined, you are welcome to spin the code into a seperate library project.

You are also welcome to modify the export logic in main.cpp to your needs, and to contribute overall patches.

## License

Photocrunch is licensed under the GNU General Public License V2. You can find a copy in the repository.

## Why do I want this?

Photocrunch lets your artists work with psd files, and you can instantly export their work as psds that can be atlassed by TexturePacker or any other tool of your choice. The information saved in the xml can be used by your game or application to work with the original positioning in the PSD. This makes it very easy for instance to create a hidden object scenes parser.

The tool and earlier variations have been used in dozens of shipped titles to accelerate the workflow between programmers and artists. Rather than letting the code rot on my harddrive I decided to contribute it.

## That's all very nice. How do I use it?

1. On Windows

On the command line (cmd) or in a batch file:
bin\windows\photocrunch.exe -i input.psd -o outputfolder

2. On Mac

On the command line (Terminal) or in a bash script:
bin/macosx/photocrunch -i input.psd -o outputfolder



