/**
 * PSD document parser - definitions
 *
 * Copyright (C) 2010-2015 Emmanuel Marty
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef  PSD_DOCUMENT_H
#define  PSD_DOCUMENT_H

#include "Common.h"
#include "PsdLayerImage.h"

/** PSD channel */
typedef struct {
   short nChannelId;
   size_t nChannelDataSize;
   size_t nChannelDataOffset;
} PsdChannel;

enum {
   PSD_ADJUSTMENT_NONE = -1,
   PSD_ADJUSTMENT_EXPOSURE = 0,
};

/** PSD layer */
typedef struct {
   char szName[256];

   bool bLayerIdKnown;
   unsigned long nLayerId;

   long nParentLayerIdx;
   long nGroupControlLayerIdx;

   long nLeft;
   long nTop;
   long nBottom;
   long nRight;
   
   long nBlendingMode;
   long nOpacity;
   long nClipping;
   unsigned long nFlags;

   bool bMaskEnabled;
   long nMaskLeft;
   long nMaskTop;
   long nMaskBottom;
   long nMaskRight;
   long nMaskDefaultColor;
   unsigned long nMaskFlags;

   long nChannels;
   PsdChannel *channel;

   PsdLayerImage layerImg;

   long nAdjustmentType;
   
   float fExposureValue;
   float fExposureOffset;
   float fExposureGamma;
} PsdLayer;

/** Auxiliary image-wide channel */

typedef struct {
   char szName[256];
} PsdAuxChannel;

/** PSD document */

class PsdDocument {
public:
   /** Constructor */
   PsdDocument ();

   /** Destructor */
   ~PsdDocument ();

   /**
    * Parse PSD file
    *
    * @param lpData psd file contents; contents are not copied and must not be freed until this document is deleted
    * @param nDataSize psd file size
    *
    * @return true for success, false for failure
    */
   bool parsePsd (const unsigned char *lpData, size_t nDataSize);
   
   /**
    * Get document width, in pixels
    *
    * @return document width
    */
   long getWidth (void);

   /**
    * Get document height, in pixels
    *
    * @return document height
    */
   long getHeight (void);

   /**
    * Get number of layers in document
    *
    * @return number of layers
    */
   long getLayerCount (void);

   /**
    * Get layer information
    *
    * @param nLayerIdx index of layer to get information for
    *
    * @return layer info
    */
   const PsdLayer *getLayerInfo (long nLayerIdx);

   /** 
    * Set layer flags
    *
    * @param nLayerIdx index of layer to set flags for
    * @param nFlags new flags
    */
   void setLayerFlags (long nLayerIdx, unsigned long nFlags);

   /**
    * Check if specified layer is visible (the layer itself and all its parent layers in layer groups must be visible)
    *
    * @param nLayerIdx index of layer to be checked
    *
    * @return true if visible, false if not
    */
   bool isLayerVisible (long nLayerIdx);

   /**
    * Get absolute opacity of layer (the layer's combined with all its parents layer's opacities)
    *
    * @param nLayerIdx indxe of layer to get absolute opacity of
    *
    * @return opacity
    */
   long getAbsOpacity (long nLayerIdx);

   /**
    * Composite specified layer contents into specified image
    *
    * @param lpImage image to composite layer in
    * @param nLayerIdx index of layer to be composited in
    * @param bDocPlacement true to respect position of layer in the document (default), false to always compose the layer at (0,0) - useful to retrieve layers that are partially offscreen
    */
   void compositeLayer (PsdLayerImage *lpImage, long nLayerIdx, bool bDocPlacement = true);

   /** Free resources used by this PSD file */
   void freePsd (void);

private:
   /**
    * Decompress pixel data of the specified layer
    *
    * @param lpImgPixels current pixels of image that this layer will be composited in; used to calculate adjustment layers
    * @param lpLayer layer to decompress
    */
   void decompressLayerPixels (unsigned int *lpImgPixels, PsdLayer *lpLayer);

   /**
    * Decompress background pixel data of the specified layer
    *
    * @param nOffset starting offset of image data in file
    * @param lpImage image to decompress into
    */
   void decompressBackgroundPixels (size_t nOffset, PsdLayerImage *lpImage);

   /**
    * Read 16-bit big endian word
    *
    * @param lpBytes location
    *
    * @return 16-bit word in native endianness
    */
   static short readPsdShort (const unsigned char *lpBytes);

   /**
    * Read 32-bit big endian word
    *
    * @param lpBytes location
    *
    * @return 32-bit word in native endianness
    */
   static long readPsdLong (const unsigned char *lpBytes);

   /**
    * Convert rgb value to lch value
    *
    * @param r red component
    * @param g green component
    * @param b blue component
    * @param luma returned lightness
    * @param chroma returned chroma
    * @param hue returned hue
    */
   static void rgbToLch (float r, float g, float b, float &luma, float &chroma, float &hue);

   /**
    * Convert lch value to rgb value
    *
    * @param luma lightness
    * @param chroma chroma
    * @param hue hue
    * @param r returned red component
    * @param g returned green component
    * @param b returned blue component
    */
   static void lchToRgb (float luma, float chroma, float hue, float &r, float &g, float &b);

   /** PSD file contents */
   const unsigned char *_lpData;

   /** PSD file size */
   size_t _nDataSize;

   /** Color palette, for indexed color */
   unsigned int *_lpPalette;

   /** Number of colors in palette */
   unsigned long _nPaletteEntries;

   /** Number of channels */
   short _nChannels;

   /** Number of bits per pixel */
   short _nBpp;

   /** Image's color mode */
   short _nColorMode;

   /** Width in pixels */
   long _nWidth;

   /** Height in pixels */
   long _nHeight;

   /** true if first alpha channel contains the transparency data for the merged result */
   bool _bAbsoluteAlpha;

   /** Number of auxiliary image-wide channels */
   long _nAuxChannels;

   /** Auxiliary image-wide channels */
   PsdAuxChannel *_auxChannel;

   /** Number of layers */
   long _nLayers;

   /** Information for each layer */
   PsdLayer *_layer;
};

#endif   /* PSD_DOCUMENT_H */
