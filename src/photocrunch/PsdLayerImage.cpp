/**
 * Kanji rendering and I/O engine - image reader/writer implementation
 *
 * Copyright (C) 2010-2015 Emmanuel Marty
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#include "PsdLayerImage.h"

/**** PUBLIC ****/

/** Constructor */

PsdLayerImage::PsdLayerImage () :
      _nImageWidth (0),
      _nImageHeight (0),
      _bGreyscale (false),
      _bHiQuality (true),
      _bAlphaChannelPresent (false),
      _lpPixels (NULL) {
}

/** Destructor */

PsdLayerImage::~PsdLayerImage () {
   freeImage ();
}

/** Free resources used by this image */

void PsdLayerImage::freeImage (void) {
   if (_lpPixels) {
      delete [] _lpPixels;
      _lpPixels = NULL;
   }
}

/**
 * Get this image's width
 *
 * \return width in pixels
 */

long PsdLayerImage::getImageWidth (void) const {
   return _nImageWidth;
}

/**
 * Get this image's height
 *
 * \return height in pixels
 */

long PsdLayerImage::getImageHeight (void) const {
   return _nImageHeight;
}

/**
 * Get this image's contents, as an array of ABGR pixels
 *
 * \return pixel data
 */

unsigned int *PsdLayerImage::getPixels (void) const {
   return _lpPixels;
}

/**
 * Set this image's contents, as an array of ABGR pixels
 *
 * \param nWidth image width, in pixels
 * \param nHeight image height, in pixels
 * \param bAlphaChannelPresent true if alpha channel is present, false if not
 * \param lpPixels image contents, as an array of ABGR pixels; the buffer is not copied but will be freed when this image is destroyed
 * \param bGreyscale true if image was created from greyscale data
 * \param bHiQuality true if image was created from 8 bits per color channel data, false it was created from less (for instance 15/16 bits per pixel images)
 */

void PsdLayerImage::setPixels (unsigned long nWidth, unsigned long nHeight, bool bAlphaChannelPresent, unsigned int *lpPixels, bool bGreyscale, bool bHiQuality) {
   _nImageWidth = nWidth;
   _nImageHeight = nHeight;
   _bAlphaChannelPresent = bAlphaChannelPresent;
   _lpPixels = lpPixels;
   _bGreyscale = bGreyscale;
   _bHiQuality = bHiQuality;
}

/** Clear pixels that are entirely invisible, so as to save a smaller compressed image */

void PsdLayerImage::optimizeImage (void) {
   long x, y;

   if (!_nImageWidth || !_nImageHeight || !_lpPixels) return;

   for (y = 0; y < _nImageHeight; y++) {
      for (x = 0; x < _nImageWidth; x++) {
         unsigned int nPixel;

         nPixel = _lpPixels[y * _nImageWidth + x];

         if (y > 1) {
            if (x > 1) nPixel |= _lpPixels[(y - 2) * _nImageWidth + (x - 2)];
            if (x > 0) nPixel |= _lpPixels[(y - 2) * _nImageWidth + (x - 1)];
            nPixel |= _lpPixels[(y - 2) * _nImageWidth + x];
            if (x < (_nImageWidth - 1)) nPixel |= _lpPixels[(y - 2) * _nImageWidth + (x + 1)];
            if (x < (_nImageWidth - 2)) nPixel |= _lpPixels[(y - 2) * _nImageWidth + (x + 2)];
         }

         if (y > 0) {
            if (x > 1) nPixel |= _lpPixels[(y - 1) * _nImageWidth + (x - 2)];
            if (x > 0) nPixel |= _lpPixels[(y - 1) * _nImageWidth + (x - 1)];
            nPixel |= _lpPixels[(y - 1) * _nImageWidth + x];
            if (x < (_nImageWidth - 1)) nPixel |= _lpPixels[(y - 1) * _nImageWidth + (x + 1)];
            if (x < (_nImageWidth - 2)) nPixel |= _lpPixels[(y - 1) * _nImageWidth + (x + 2)];
         }

         if (x > 1) nPixel |= _lpPixels[y * _nImageWidth + (x - 2)];
         if (x > 0) nPixel |= _lpPixels[y * _nImageWidth + (x - 1)];
         if (x < (_nImageWidth - 1)) nPixel |= _lpPixels[y * _nImageWidth + (x + 1)];
         if (x < (_nImageWidth - 2)) nPixel |= _lpPixels[y * _nImageWidth + (x + 2)];

         if (y < (_nImageHeight - 1)) {
            if (x > 1) nPixel |= _lpPixels[(y + 1) * _nImageWidth + (x - 2)];
            if (x > 0) nPixel |= _lpPixels[(y + 1) * _nImageWidth + (x - 1)];
            nPixel |= _lpPixels[(y + 1) * _nImageWidth + x];
            if (x < (_nImageWidth - 1)) nPixel |= _lpPixels[(y + 1) * _nImageWidth + (x + 1)];
            if (x < (_nImageWidth - 2)) nPixel |= _lpPixels[(y + 1) * _nImageWidth + (x + 2)];
         }

         if (y < (_nImageHeight - 2)) {
            if (x > 1) nPixel |= _lpPixels[(y + 2) * _nImageWidth + (x - 2)];
            if (x > 0) nPixel |= _lpPixels[(y + 2) * _nImageWidth + (x - 1)];
            nPixel |= _lpPixels[(y + 2) * _nImageWidth + x];
            if (x < (_nImageWidth - 1)) nPixel |= _lpPixels[(y + 2) * _nImageWidth + (x + 1)];
            if (x < (_nImageWidth - 2)) nPixel |= _lpPixels[(y + 2) * _nImageWidth + (x + 2)];
         }

         if ((nPixel & 0xff000000) == 0)
            _lpPixels[y * _nImageWidth + x] = 0;
      }
   }
}

/**
 * Trim image to the part that contains visible pixels
 *
 * @param nPadding number of padding pixels to add around the trimmed image, if any
 * @param rx1 returned leftmost coordinate of trimmed region, in original image coordinates
 * @param ry1 returned topmost coordinate of trimmed region, in original image coordinates
 * @param rx2 returned rightmost coordinate of trimmed region, in original image coordinates, +1
 * @param ry2 returned bottommost coordinate of trimmed region, in original image coordinates, +1
 */

void PsdLayerImage::trimImage(long nPadding, long &rx1, long &ry1, long &rx2, long &ry2) {
   long i, nXMin, nYMin, nXMax, nYMax, x1, y1, x2, y2, x, y, nBoundedWidth, nBoundedHeight;
   unsigned int *lpTrimmedPixels;

   if (!_nImageWidth || !_nImageHeight || !_lpPixels) return;

   nXMin = 0;
   nYMin = 0;
   nXMax = _nImageWidth;
   nYMax = _nImageHeight;
   x1 = _nImageWidth + 1;
   y1 = _nImageHeight + 1;
   x2 = nXMin - 1;
   y2 = nYMin - 1;

   /* Find bounding rectangle */
   for (y = nYMin; y < nYMax; y++) {
      for (x = nXMin; x < nXMax; x++) {
         if (_lpPixels[x + y * _nImageWidth] & 0xff000000) {
            if (x1 > x) x1 = x;
            if (y1 > y) y1 = y;
            if (x2 < x) x2 = x;
            if (y2 < y) y2 = y;
         }
      }
   }

   if (x2 < x1 || y2 < y1) {
      x1 = nXMin;
      y1 = nYMin;
      x2 = nXMin;
      y2 = nYMin;
   }
   else {
      x1 = x1 - nPadding;
      y1 = y1 - nPadding;
      x2 = x2 + nPadding;
      y2 = y2 + nPadding;
      if (x1 < nXMin) x1 = nXMin;
      if (y1 < nYMin) y1 = nYMin;
      if (x2 > nXMax) x2 = nXMax;
      if (y2 > nYMax) y2 = nYMax;
   }

   nBoundedWidth = x2 - x1;
   nBoundedHeight = y2 - y1;
   lpTrimmedPixels = new unsigned int[nBoundedWidth * nBoundedHeight];
   memset(lpTrimmedPixels, 0, sizeof(unsigned int) * nBoundedWidth * nBoundedHeight);

   for (i = y1; i < y2; i++) {
      memcpy(lpTrimmedPixels + (i - y1) * nBoundedWidth, _lpPixels + (i * _nImageWidth + x1), nBoundedWidth * sizeof(unsigned int));
   }

   delete[] _lpPixels;
   _lpPixels = NULL;

   _nImageWidth = nBoundedWidth;
   _nImageHeight = nBoundedHeight;
   _lpPixels = lpTrimmedPixels;

   rx1 = x1;
   ry1 = y1;
   rx2 = x2;
   ry2 = y2;
}
