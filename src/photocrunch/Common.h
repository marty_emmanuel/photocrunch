/**
 * Common definitions
 *
 * Copyright (C) 2010-2015 Emmanuel Marty
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifndef  _COMMON_H
#define  _COMMON_H

#define NOMINMAX
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#ifdef _WIN32
#include <windows.h>
#include	<shlobj.h>
#include <shlwapi.h>
#endif
#ifdef __APPLE__
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#endif
#include <math.h>
#include <time.h>
#include <sys/stat.h>
#include <sys/types.h>

#endif   /* _COMMON_H */
