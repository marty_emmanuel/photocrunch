/**
 * PSD document parser - implementation
 *
 * Copyright (C) 2010-2015 Emmanuel Marty
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

#ifdef _MSC_VER
#pragma warning (disable : 4996)
#endif

#include "PsdDocument.h"

/** Minimum of two values */
#define PSD_MIN(__a,__b) (((__a) < (__b)) ? (__a) : (__b))

/** Maximum of two values */
#define PSD_MAX(__a,__b) (((__a) > (__b)) ? (__a) : (__b))

/** Copy bounded string safely */
#define PsdStringCpy(__buffer,__str,__len) do { strncpy (__buffer,__str,__len); __buffer[(__len)-1] = 0; } while (0)

/** Log debug traces */
#ifdef _PSD_DEBUG
#define PSD_LOG(...) do { fprintf (stdout, __VA_ARGS__); fprintf (stdout, "\n"); fflush (stdout); } while (0)
#else
#define PSD_LOG(...)
#endif

/** Apply linear burn blending */
#define ADD_BLEND(_VAL1,_VAL2) ((unsigned int)((_VAL2 + _VAL1 > 255) ? 255:(_VAL2 + _VAL1)))

/** Apply linear dodge blending */
#define SUBTRACT_BLEND(_VAL1,_VAL2)	((unsigned int)((_VAL2 + _VAL1 < 255) ? 0:(_VAL2 + _VAL1 - 255)))

/** Apply lighten blending */
#define LIGHTEN_BLEND(_VAL1,_VAL2) ((unsigned int)((_VAL1 > _VAL2) ? _VAL1:_VAL2))

/** Apply darken blending */
#define DARKEN_BLEND(_VAL1,_VAL2) ((unsigned int)((_VAL1 > _VAL2) ? _VAL2:_VAL1))

/** Apply exclusion blending */
#define EXCLUSION_BLEND(_VAL1,_VAL2) ((unsigned int)(_VAL2 + _VAL1 - 2 * _VAL2 * _VAL1 / 255))

/** Apply overlay blending */
#define OVERLAY_BLEND(_VAL1,_VAL2) (_VAL1 < 128) ? (2 *(_VAL1 * _VAL2 / 255)) : (255 - 2 * (255 - _VAL1) * (255 - _VAL2) / 255)

/** Apply multiply blending */
#define MULTIPLY_BLEND(_VAL1,_VAL2) (_VAL2 * _VAL1 / 255)

/** Apply screen blending */
#define SCREEN_BLEND(_VAL1,_VAL2) 255 - (255 - _VAL2) * (255 - _VAL1) / 255

/* Apply color burn blending */
#define COLOR_BURN_BLEND(_VAL1,_VAL2) ((unsigned int)((_VAL2 == 0) ? 0:((255 - (((255 - _VAL1) << 8 ) / _VAL2)) < 0) ? 0:(255 - (((255 - _VAL1) << 8 ) / _VAL2))))

/** Apply color dodge blending */
#define COLOR_DODGE_BLEND(_VAL1,_VAL2) ((unsigned int)((_VAL2 == 255) ? _VAL2 : ((_VAL1 << 8) / (255 - _VAL2) > 255) ? 255 : ((_VAL1 << 8) / (255 - _VAL2))))

/** Apply soft light blending */
#define SOFT_LIGHT_BLEND(_VAL1,_VAL2) ((unsigned int) ((_VAL2 * _VAL1) >> 8) + ((_VAL1 * (255 - (((255 - _VAL1) * (255-_VAL2)) >> 8) - ((_VAL2 * _VAL1) >> 8))) >> 8))

/** Apply linear light blending */
#define LINEAR_LIGHT_BLEND(_VAL1,_VAL2) ((unsigned int)(_VAL2 < 128) ? SUBTRACT_BLEND(_VAL1,(2 * _VAL2)):ADD_BLEND(_VAL1,(2 * (_VAL2 - 128))))

/** Apply vivid light blending */
#define VIVID_LIGHT_BLEND(_VAL1,_VAL2)	((unsigned int)(_VAL2 < 128) ? COLOR_BURN_BLEND(_VAL1,(2 * _VAL2)):COLOR_DODGE_BLEND(_VAL1,(2 * (_VAL2 - 128))))

/** Apply pin light blending */
#define PIN_LIGHT_BLEND(_VAL1,_VAL2) ((unsigned int)(_VAL2 < 128) ? DARKEN_BLEND((2 * _VAL2),_VAL1):LIGHTEN_BLEND((2 *(_VAL2 - 128)),_VAL1))

/**** PUBLIC ****/

/** Constructor */

PsdDocument::PsdDocument () :
      _lpData (NULL),
      _nDataSize (0),
      _lpPalette (NULL),
      _nPaletteEntries (0),
      _nChannels (0),
      _nBpp (0),
      _nColorMode (-1),
      _nWidth (0),
      _nHeight (0),
      _nAuxChannels (0),
      _auxChannel (NULL),
      _nLayers (0),
      _layer (NULL) {
}

/** Destructor */
PsdDocument::~PsdDocument () {
   freePsd ();
}

/**
 * Parse PSD file
 *
 * \param lpData psd file contents; contents are not copied and must not be freed until this document is deleted
 * \param nDataSize psd file size
 *
 * \return true for success, false for failure
 */

bool PsdDocument::parsePsd (const unsigned char *lpData, size_t nDataSize) {
   const unsigned char *lpCurData = lpData, *lpPaletteEnd, *lpImgResourceEnd, *lpLayerMaskEnd;
   short nVersion;
   long nSize;
   bool bResourcesDone;

   freePsd ();

   /* Read 8BPS header */

   if (lpCurData[0] != '8' || lpCurData[1] != 'B' || lpCurData[2] != 'P' || lpCurData[3] != 'S') return false;
   nVersion = readPsdShort (lpCurData + 4);
   if (nVersion != 1) return false;

   _lpData = lpData;
   _nDataSize = nDataSize;

   _nChannels = readPsdShort (lpCurData + 12);
   _nHeight = readPsdLong (lpCurData + 14);
   _nWidth = readPsdLong (lpCurData + 18);
   _nBpp = readPsdShort (lpCurData + 22);
   _nColorMode = readPsdShort (lpCurData + 24);
   _lpPalette = NULL;
   _nPaletteEntries = 0;
   _bAbsoluteAlpha = false;
   lpCurData += 26;

   PSD_LOG ("PSD: %ld x %ld x %ld image, %ld channels, color mode %ld", _nWidth, _nHeight, _nBpp, _nChannels, _nColorMode);

   /* Read color mode data block */

   nSize = readPsdLong (lpCurData);
   lpCurData += 4;
   lpPaletteEnd = lpCurData + nSize;

   if (_nColorMode == 2) {
      unsigned long i;

      /* Indexed color */
      _nPaletteEntries = nSize / 3;
      _lpPalette = new unsigned int [_nPaletteEntries];
      for (i = 0; i < _nPaletteEntries; i++) {
         unsigned int r = (unsigned int) lpCurData[0], g = (unsigned int) lpCurData[_nPaletteEntries], b = (unsigned int) lpCurData[_nPaletteEntries*2];
         _lpPalette[i] = r | (g << 8) | (b << 16);

         lpCurData ++;
      }
   }

   lpCurData = lpPaletteEnd;

   /* Read image resources block */

   nSize = readPsdLong (lpCurData);
   lpCurData += 4;
   lpImgResourceEnd = lpCurData + nSize;

   bResourcesDone = false;
   while (!bResourcesDone && lpCurData < lpImgResourceEnd) {
      bResourcesDone = true;

      if (lpCurData[0] == '8' && lpCurData[1] == 'B' && lpCurData[2] == 'I' && lpCurData[3] == 'M') {
         short nResourceId;
         long nStrLen, nRemainingSize;
         const unsigned char *lpResData;

         lpCurData += 4;
         nResourceId = readPsdShort (lpCurData);
         lpCurData += 2;
         nSize = *lpCurData++;
         lpCurData += nSize;
         if ((lpCurData - lpData) & 1) {
            /* Align on word */
            lpCurData++;
         }
         nSize = readPsdLong (lpCurData);
         lpCurData += 4;

         PSD_LOG ("PSD: resource type 0x%04lX size 0x%08lX", nResourceId, nSize);

         switch (nResourceId) {
         case 1006:
            lpResData = lpCurData;
            nRemainingSize = nSize;

            /* Aux channel names */

            do {
               long nCopyLen;

               nStrLen = (long) ((unsigned long) *lpResData++);
               nRemainingSize--;
               if (nStrLen > nRemainingSize)
                  nStrLen = nRemainingSize;
               nCopyLen = nStrLen;
               if (nCopyLen > 255) nCopyLen = 255;

               _auxChannel = (PsdAuxChannel *) realloc (_auxChannel, sizeof (PsdAuxChannel) * (_nAuxChannels + 1));
               memcpy (_auxChannel[_nAuxChannels].szName, lpResData, nCopyLen);
               _auxChannel[_nAuxChannels].szName[nCopyLen] = 0;

               PSD_LOG ("PSD: aux channel %ld: '%s'", _nAuxChannels, _auxChannel[_nAuxChannels].szName);

               _nAuxChannels++;

               lpResData += nStrLen;
               nRemainingSize -= nStrLen;
            } while (nRemainingSize > 0);
            break;

         default:
            break;
         }

         if (nSize & 1)
            nSize++;
         lpCurData += nSize;
         bResourcesDone = false;
      }
   }

   /* Read layer and mask information block */

   lpCurData = lpImgResourceEnd;
   nSize = readPsdLong (lpCurData);
   lpCurData += 4;
   lpLayerMaskEnd = lpCurData + nSize;

   if (nSize >= 4) {
      const unsigned char *lpLayerEnd, *lpMaskEnd;
      short i, nLayerRecords;
      long *nParentLayerStack, nParentLayerStackSize;

      /* Block isn't empty; read size of layer information */
      nSize = readPsdLong (lpCurData);
      lpCurData += 4;
      lpLayerEnd = lpCurData + nSize;

      nLayerRecords = readPsdShort (lpCurData);
      lpCurData += 2;

      if (nLayerRecords < 0) {
         nLayerRecords = -nLayerRecords;
         _bAbsoluteAlpha = true;
         PSD_LOG ("PSD: uses absolute alpha");
      }

      _nLayers = (long) nLayerRecords;
      _layer = new PsdLayer [_nLayers];

      nParentLayerStack = new long [_nLayers+1];
      nParentLayerStack[0] = -1;
      nParentLayerStackSize = 0;

      i = 0;
      while (lpCurData < lpLayerEnd && i < nLayerRecords) {
         PsdLayer *lpCurLayer = &_layer[i];
         short j, nLayerChannels;

         /* Read layer header */

         lpCurLayer->bLayerIdKnown = false;
         lpCurLayer->nLayerId = 0;
         lpCurLayer->nParentLayerIdx = nParentLayerStack[nParentLayerStackSize];
         lpCurLayer->nGroupControlLayerIdx = -1;
         lpCurLayer->nTop = readPsdLong (lpCurData); lpCurData += 4;
         lpCurLayer->nLeft = readPsdLong (lpCurData); lpCurData += 4;
         lpCurLayer->nBottom = readPsdLong (lpCurData); lpCurData += 4;
         lpCurLayer->nRight = readPsdLong (lpCurData); lpCurData += 4;
         nLayerChannels = readPsdShort (lpCurData); lpCurData += 2;
         lpCurLayer->nChannels = nLayerChannels;
         lpCurLayer->channel = new PsdChannel [nLayerChannels];
         lpCurLayer->nAdjustmentType = -1;
         lpCurLayer->fExposureValue = lpCurLayer->fExposureOffset = lpCurLayer->fExposureGamma = 0;

         /* Read layer channels */

         j = 0;
         while (lpCurData < lpLayerEnd && j < nLayerChannels) {
            lpCurLayer->channel[j].nChannelId = readPsdShort (lpCurData); lpCurData += 2;   /* 0=red, 1=green, 2=blue, etc.; -1=transparency mask; -2=user supplied layer mask */
            lpCurLayer->channel[j].nChannelDataSize = readPsdLong (lpCurData); lpCurData += 4;
            j++;
         }

         if (lpCurData[0] == '8' && lpCurData[1] == 'B' && lpCurData[2] == 'I' && lpCurData[3] == 'M') {
            const unsigned char *lpExtraDataEnd;
            size_t nNameLen;

            lpCurData += 4;
            lpCurLayer->nBlendingMode = readPsdLong (lpCurData); lpCurData += 4;
            lpCurLayer->nOpacity = (long) ((unsigned long) (*lpCurData++));
            lpCurLayer->nClipping = (long) ((unsigned long) (*lpCurData++));
            lpCurLayer->nFlags = (long) ((unsigned long) (*lpCurData++));
            lpCurData++;
            nSize = readPsdLong (lpCurData); lpCurData += 4;   /* Total extra data size */
            lpExtraDataEnd = lpCurData + nSize;

            /* Read mask data */
            lpCurLayer->bMaskEnabled = false;
            lpCurLayer->nMaskLeft = lpCurLayer->nMaskTop = lpCurLayer->nMaskRight = lpCurLayer->nMaskBottom = -1;
            lpCurLayer->nMaskDefaultColor = 0;
            lpCurLayer->nMaskFlags = 0;
            nSize = readPsdLong (lpCurData); lpCurData += 4;
            if (nSize >= 0x14) {
               lpCurLayer->nMaskTop = readPsdLong (lpCurData);
               lpCurLayer->nMaskLeft = readPsdLong (lpCurData + 4);
               lpCurLayer->nMaskBottom = readPsdLong (lpCurData + 8);
               lpCurLayer->nMaskRight = readPsdLong (lpCurData + 12);
               lpCurLayer->nMaskDefaultColor = (long) ((unsigned long) lpCurData[16]);
               lpCurLayer->nMaskFlags = (unsigned long) lpCurData[17];
               lpCurLayer->bMaskEnabled = true;
            }
            lpCurData += nSize;

            /* Read blending ranges data */
            nSize = readPsdLong (lpCurData); lpCurData += 4;
            lpCurData += nSize;

            /* Read layer name */
            nSize = (size_t) *lpCurData;
            memset (lpCurLayer->szName, 0, 256);
            nNameLen = nSize;
            if (nNameLen > 255) nNameLen = 255;
            memcpy (lpCurLayer->szName, lpCurData + 1, nNameLen);
            lpCurLayer->szName[nNameLen] = 0;
            nSize++;
            nSize = (nSize + 3) & (~3);
            lpCurData += nSize;

            PSD_LOG ("PSD: layer %d: '%s' at %ld,%ld-%ld,%ld, %ld channels, blending mode: %c%c%c%c, opacity: %f%% visible: %s flags: 0x%02X parent: %ld", i, lpCurLayer->szName, lpCurLayer->nLeft, lpCurLayer->nTop, lpCurLayer->nRight, lpCurLayer->nBottom, nLayerChannels,
                   lpCurLayer->nBlendingMode >> 24, (lpCurLayer->nBlendingMode >> 16) & 255, (lpCurLayer->nBlendingMode >> 8) & 255, lpCurLayer->nBlendingMode & 255,
                   lpCurLayer->nOpacity * 100.0f / 255.0f,
                   (lpCurLayer->nFlags & 2) ? "NO":"yes", lpCurLayer->nFlags, lpCurLayer->nParentLayerIdx);

            /* More 8BIM records follow here */

            while (lpCurData < lpExtraDataEnd) {
               if (lpCurData[0] == '8' && lpCurData[1] == 'B' && lpCurData[2] == 'I' && lpCurData[3] == 'M') {
                  long nKey;
                  unsigned long nValue;
                  
                  nKey = readPsdLong (lpCurData + 4);                  
                  lpCurData += 8;
                  nSize = readPsdLong (lpCurData);
                  lpCurData += 4;

                  PSD_LOG ("    Layer has %c%c%c%c record", nKey >> 24, (nKey >> 16) & 255, (nKey >> 8) & 255, nKey & 255);

                  switch (nKey) {
                  case ('l'<<24)|('s'<<16)|('c'<<8)|('t'):
                     /* Layer set controls type */
                     nValue = readPsdLong (lpCurData);
                     switch (nValue) {
                     case 1:
                     case 2:
                        /* Close folder */
                        if (nParentLayerStackSize > 0) {
                           _layer[nParentLayerStack[nParentLayerStackSize]].nGroupControlLayerIdx = i;
                           nParentLayerStackSize--;
                        }
                        break;

                     case 3:
                        /* Open folder */
                        nParentLayerStack[++nParentLayerStackSize] = i;
                        break;

                     default:
                        /* Unsupported */
                        break;
                     }
                     break;

                  case ('e'<<24)|('x'<<16)|('p'<<8)|('A'):
                     /* Exposure adjustment layer */
                     {
                        short nVersion = readPsdShort (lpCurData);
                        long nExposure = readPsdLong (lpCurData+2);
                        long nOffset = readPsdLong (lpCurData+6);
                        long nGamma = readPsdLong (lpCurData+10);

                        lpCurLayer->fExposureValue = *((float *) &nExposure);
                        lpCurLayer->fExposureOffset = *((float *) &nOffset);
                        lpCurLayer->fExposureGamma = *((float *) &nGamma);
                        lpCurLayer->nAdjustmentType = PSD_ADJUSTMENT_EXPOSURE;
                     }
                     break;

                  case ('l'<<24)|('u'<<16)|('n'<<8)|('i'):
                     /* Unicode layer name */
                     {
                        long nUnicodeNameLen = readPsdLong (lpCurData), n;
                        const unsigned char *lpCurUnicodeNameData = lpCurData + 4;

                        nNameLen = 0;
                        for (n = 0; n < nUnicodeNameLen; n++) {
                           short nUnicodeChar = readPsdShort (lpCurUnicodeNameData);
                           
                           lpCurUnicodeNameData += 2;
                           if (nUnicodeChar > 255) nUnicodeChar = '?';
                           if (nNameLen < 255)
                              lpCurLayer->szName[nNameLen++] = (char) nUnicodeChar;
                        }
                        lpCurLayer->szName[nNameLen] = 0;
                     }
                     break;

                  case ('l'<<24)|('y'<<16)|('i'<<8)|('d'):
                     /* Layer ID */
                     {
                        long nLayerId = readPsdLong (lpCurData);

                        lpCurLayer->bLayerIdKnown = true;
                        lpCurLayer->nLayerId = (unsigned long) nLayerId;
                     }
                     break;

                  default:
                     break;
                  }

                  lpCurData += nSize;
               }
               else {
                  lpCurData = lpExtraDataEnd;
               }

            }

            lpCurData = lpExtraDataEnd;
         }
         else {
            lpCurData = lpLayerEnd;
         }

         i++;
      }

      for (i = 0; i < _nLayers; i++) {
         PsdLayer *lpCurLayer = &_layer[i];
         long j;

         for (j = 0; j < lpCurLayer->nChannels; j++) {
            lpCurLayer->channel[j].nChannelDataOffset = lpCurData - lpData;
            lpCurData += lpCurLayer->channel[j].nChannelDataSize;

            PSD_LOG ("Layer %s: channel %ld offset is %ld, compression type 0x%04X", lpCurLayer->szName, lpCurLayer->channel[j].nChannelId, lpCurLayer->channel[j].nChannelDataOffset,
                   readPsdShort (lpData + lpCurLayer->channel[j].nChannelDataOffset));
         }
      }

      lpCurData = lpLayerEnd;

      /* Read mask information */

      nSize = readPsdLong (lpCurData);
      lpCurData += 4;
      lpMaskEnd = lpCurData + nSize;

      /* More 8BIM records follow here */

      while (lpCurData < lpLayerMaskEnd) {
         if (lpCurData[0] == '8' && lpCurData[1] == 'B' && lpCurData[2] == 'I' && lpCurData[3] == 'M') {
            long nKey;
                  
            nKey = readPsdLong (lpCurData + 4);                  
            lpCurData += 8;
            nSize = readPsdLong (lpCurData);
            lpCurData += 4;

            PSD_LOG ("Image has %c%c%c%c record", nKey >> 24, (nKey >> 16) & 255, (nKey >> 8) & 255, nKey & 255);

            lpCurData += nSize;
         }
         else {
            lpCurData = lpLayerMaskEnd;
         }

      }
   }
   else {
      /* No layers */
      _nLayers = 0;
      _layer = new PsdLayer [_nLayers];
   }

   lpCurData = lpLayerMaskEnd;
   if (!_nLayers) {
      PsdLayer *lpCurLayer;

      /* No layers; decompress background image data */
 
      _nLayers = 1;
      if (_layer) delete [] _layer;
      _layer = new PsdLayer [_nLayers];
      lpCurLayer = &_layer[0];
      PsdStringCpy (lpCurLayer->szName, "background", 256);
      lpCurLayer->nLeft = 0;
      lpCurLayer->nTop = 0;
      lpCurLayer->nRight = _nWidth;
      lpCurLayer->nBottom = _nHeight;
      lpCurLayer->nBlendingMode = ('n'<<24)|('o'<<16)|('r'<<8)|('m');
      lpCurLayer->nOpacity = 255;
      lpCurLayer->nClipping = 0;
      lpCurLayer->nFlags = 0;
      lpCurLayer->bMaskEnabled = false;
      lpCurLayer->nMaskLeft = lpCurLayer->nMaskTop = lpCurLayer->nMaskRight = lpCurLayer->nMaskBottom = -1;
      lpCurLayer->nMaskDefaultColor = 0;
      lpCurLayer->nMaskFlags = 0;
      lpCurLayer->nChannels = 0;
      lpCurLayer->channel = NULL;

      decompressBackgroundPixels (lpCurData - _lpData, &lpCurLayer->layerImg);
   }

   return true;
}

/**
 * Get document width, in pixels
 *
 * \return document width
 */

long PsdDocument::getWidth (void) {
   return _nWidth;
}

/**
 * Get document height, in pixels
 *
 * \return document height
 */

long PsdDocument::getHeight (void) {
   return _nHeight;
}

/**
 * Get number of layers in document
 *
 * \return number of layers
 */

long PsdDocument::getLayerCount (void) {
   return _nLayers;
}

/**
 * Get layer information
 *
 * \param nLayerIdx index of layer to get information for
 *
 * \return layer info
 */

const PsdLayer *PsdDocument::getLayerInfo (long nLayerIdx) {
   if (nLayerIdx < 0 || nLayerIdx > _nLayers || !_layer) return NULL;

   return &_layer[nLayerIdx];
}

/** 
 * Set layer flags
 *
 * \param nLayerIdx index of layer to set flags for
 * \param nFlags new flags
 */

void PsdDocument::setLayerFlags (long nLayerIdx, unsigned long nFlags) {
   if (nLayerIdx < 0 || nLayerIdx > _nLayers || !_layer) return;
   _layer[nLayerIdx].nFlags = nFlags;
}

/**
 * Check if specified layer is visible (the layer itself and all its parent layers in layer groups must be visible)
 *
 * \param nLayerIdx index of layer to be checked
 *
 * \return true if visible, false if not
 */

bool PsdDocument::isLayerVisible (long nLayerIdx) {
   if (nLayerIdx < 0 || nLayerIdx > _nLayers || !_layer) return false;

   do {
      long nCheckLayerIdx;

      if (_layer[nLayerIdx].nGroupControlLayerIdx >= 0 &&
          _layer[nLayerIdx].nGroupControlLayerIdx < _nLayers)
         nCheckLayerIdx = _layer[nLayerIdx].nGroupControlLayerIdx;
      else
         nCheckLayerIdx = nLayerIdx;

      if (_layer[nCheckLayerIdx].nFlags & 2) return false;    /* Hidden */

      nLayerIdx = _layer[nLayerIdx].nParentLayerIdx;
   } while (nLayerIdx >= 0 && nLayerIdx < _nLayers);

   /* Visible and all its hierarchy as well */
   return true;
}

/**
 * Get absolute opacity of layer (the layer's combined with all its parents layer's opacities)
 *
 * \param nLayerIdx indxe of layer to get absolute opacity of
 *
 * \return opacity
 */

long PsdDocument::getAbsOpacity (long nLayerIdx) {
   long nOpacity = 255;

   if (nLayerIdx < 0 || nLayerIdx > _nLayers || !_layer) return 0;

   do {
      long nCheckLayerIdx;

      if (_layer[nLayerIdx].nGroupControlLayerIdx >= 0 &&
          _layer[nLayerIdx].nGroupControlLayerIdx < _nLayers)
         nCheckLayerIdx = _layer[nLayerIdx].nGroupControlLayerIdx;
      else
         nCheckLayerIdx = nLayerIdx;

      nOpacity = nOpacity * _layer[nCheckLayerIdx].nOpacity / 255;
      if (_layer[nLayerIdx].nParentLayerIdx == nLayerIdx)
         break;
      nLayerIdx = _layer[nLayerIdx].nParentLayerIdx;
   } while (nLayerIdx >= 0 && nLayerIdx < _nLayers);

   return nOpacity;
}

/**
 * Composite specified layer contents into specified image
 *
 * \param lpImage image to composite layer in
 * \param nLayerIdx index of layer to be composited in
 * \param bDocPlacement true to respect position of layer in the document (default), false to always compose the layer at (0,0) - useful to retrieve layers that are partially offscreen
 */

void PsdDocument::compositeLayer (PsdLayerImage *lpImage, long nLayerIdx, bool bDocPlacement) {
   PsdLayer *lpLayer;
   long x, y, nSrcWidth, nSrcHeight, nImgWidth, nImgHeight, nAbsOpacity;
   const unsigned int *lpSrcPixels;
   unsigned int *lpImgPixels;

   if (!lpImage || !lpImage->getPixels() || nLayerIdx < 0 || nLayerIdx > _nLayers) return;
   nImgWidth = lpImage->getImageWidth ();
   nImgHeight = lpImage->getImageHeight ();
   lpImgPixels = lpImage->getPixels ();

   /* First, decompress image pixels, if they aren't already available */
   lpLayer = &_layer[nLayerIdx];
   decompressLayerPixels (lpImgPixels, lpLayer);
   lpSrcPixels = lpLayer->layerImg.getPixels();
   if (!lpSrcPixels) return;
   nSrcWidth = lpLayer->layerImg.getImageWidth ();
   nSrcHeight = lpLayer->layerImg.getImageHeight ();
   nAbsOpacity = getAbsOpacity (nLayerIdx);

   switch (lpLayer->nBlendingMode) {
   case ('n'<<24)|('o'<<16)|('r'<<8)|('m'):
      /* Normal blending */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff, da = ((*lpDstPixel) >> 24) & 0xff;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;
  
               dr = (unsigned int) ((long) sr + ((long) dr - (long) sr) * (long) da / 255);
               dg = (unsigned int) ((long) sg + ((long) dg - (long) sg) * (long) da / 255);
               db = (unsigned int) ((long) sb + ((long) db - (long) sb) * (long) da / 255);

               r = dr + (sr - dr) * nBlend / 255;
               g = dg + (sg - dg) * nBlend / 255;
               b = db + (sb - db) * nBlend / 255;

               *lpDstPixel = (PSD_MAX (nBlend, da) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('o'<<24)|('v'<<16)|('e'<<8)|('r'):
      /* Overlay blending */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = OVERLAY_BLEND (dr, sr);
               vg = OVERLAY_BLEND (dg, sg);
               vb = OVERLAY_BLEND (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('i'<<24)|('d'<<16)|('i'<<8)|('v'):
      /* Color burn */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               long vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b, uvr, uvg, uvb;

               vr = COLOR_BURN_BLEND (((long) dr), ((long) sr));
               vg = COLOR_BURN_BLEND (((long) dg), ((long) sg));
               vb = COLOR_BURN_BLEND (((long) db), ((long) sb));
               uvr = (unsigned int) vr;
               uvg = (unsigned int) vg;
               uvb = (unsigned int) vb;

               r = dr + (uvr - dr) * nBlend / 255;
               g = dg + (uvg - dg) * nBlend / 255;
               b = db + (uvb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('d'<<24)|('i'<<16)|('v'<<8)|(' '):
      /* Color dodge */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = COLOR_DODGE_BLEND (dr, sr);
               vg = COLOR_DODGE_BLEND (dg, sg);
               vb = COLOR_DODGE_BLEND (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('h'<<24)|('L'<<16)|('i'<<8)|('t'):
      /* Hard light - same as overlay but reversing source and destination */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = OVERLAY_BLEND (sr, dr);
               vg = OVERLAY_BLEND (sg, dg);
               vb = OVERLAY_BLEND (sb, db);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('s'<<24)|('L'<<16)|('i'<<8)|('t'):
      /* Soft light */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = SOFT_LIGHT_BLEND (dr, sr);
               vg = SOFT_LIGHT_BLEND (dg, sg);
               vb = SOFT_LIGHT_BLEND (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('v'<<24)|('L'<<16)|('i'<<8)|('t'):
      /* Vivid light */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               long vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b, uvr, uvg, uvb;

               vr = VIVID_LIGHT_BLEND (((long) dr), ((long) sr));
               vg = VIVID_LIGHT_BLEND (((long) dg), ((long) sg));
               vb = VIVID_LIGHT_BLEND (((long) db), ((long) sb));
               uvr = (unsigned int) vr;
               uvg = (unsigned int) vg;
               uvb = (unsigned int) vb;

               r = dr + (uvr - dr) * nBlend / 255;
               g = dg + (uvg - dg) * nBlend / 255;
               b = db + (uvb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('p'<<24)|('L'<<16)|('i'<<8)|('t'):
      /* Pin light */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               long vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b, uvr, uvg, uvb;

               vr = PIN_LIGHT_BLEND (((long) dr), ((long) sr));
               vg = PIN_LIGHT_BLEND (((long) dg), ((long) sg));
               vb = PIN_LIGHT_BLEND (((long) db), ((long) sb));
               uvr = (unsigned int) vr;
               uvg = (unsigned int) vg;
               uvb = (unsigned int) vb;

               r = dr + (uvr - dr) * nBlend / 255;
               g = dg + (uvg - dg) * nBlend / 255;
               b = db + (uvb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('l'<<24)|('L'<<16)|('i'<<8)|('t'):
      /* Linear light */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               long vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b, uvr, uvg, uvb;

               vr = LINEAR_LIGHT_BLEND (((long) dr), ((long) sr));
               vg = LINEAR_LIGHT_BLEND (((long) dg), ((long) sg));
               vb = LINEAR_LIGHT_BLEND (((long) db), ((long) sb));
               uvr = (unsigned int) vr;
               uvg = (unsigned int) vg;
               uvb = (unsigned int) vb;

               r = dr + (uvr - dr) * nBlend / 255;
               g = dg + (uvg - dg) * nBlend / 255;
               b = db + (uvb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('s'<<24)|('m'<<16)|('u'<<8)|('d'):
      /* Exclusion */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = EXCLUSION_BLEND (dr, sr);
               vg = EXCLUSION_BLEND (dg, sg);
               vb = EXCLUSION_BLEND (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('l'<<24)|('i'<<16)|('t'<<8)|('e'):
      /* Lighten */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = PSD_MAX (dr, sr);
               vg = PSD_MAX (dg, sg);
               vb = PSD_MAX (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('d'<<24)|('a'<<16)|('r'<<8)|('k'):
      /* Darken */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = PSD_MIN (dr, sr);
               vg = PSD_MIN (dg, sg);
               vb = PSD_MIN (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('d'<<24)|('i'<<16)|('f'<<8)|('f'):
      /* Difference */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = abs ((long) (dr - sr));
               vg = abs ((long) (dg - sg));
               vb = abs ((long) (db - sb));
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('m'<<24)|('u'<<16)|('l'<<8)|(' '):
      /* Multiply */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = MULTIPLY_BLEND (dr, sr);
               vg = MULTIPLY_BLEND (dg, sg);
               vb = MULTIPLY_BLEND (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('s'<<24)|('c'<<16)|('r'<<8)|('n'):
      /* Screen */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = SCREEN_BLEND (dr, sr);
               vg = SCREEN_BLEND (dg, sg);
               vb = SCREEN_BLEND (db, sb);
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('l'<<24)|('b'<<16)|('r'<<8)|('n'):
      /* Linear burn */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               long vr, vg, vb;
               long uvr, uvg, uvb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = ((long) dr) + ((long) sr) - 255; if (vr < 0) vr = 0; if (vr > 255) vr = 255;
               vg = ((long) dg) + ((long) sg) - 255; if (vg < 0) vg = 0; if (vg > 255) vg = 255;
               vb = ((long) db) + ((long) sb) - 255; if (vb < 0) vb = 0; if (vb > 255) vb = 255;
               uvr = (unsigned int) vr;
               uvg = (unsigned int) vg;
               uvb = (unsigned int) vb;
  
               r = dr + (uvr - dr) * nBlend / 255;
               g = dg + (uvg - dg) * nBlend / 255;
               b = db + (uvb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('l'<<24)|('d'<<16)|('d'<<8)|('g'):
      /* Linear dodge */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               vr = dr + sr; if (vr > 255) vr = 255;
               vg = dg + sg; if (vg > 255) vg = 255;
               vb = db + sb; if (vb > 255) vb = 255;
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('s'<<24)|('a'<<16)|('t'<<8)|(' '):
      /* Saturation */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               float sl, sc, sh, dl, dc, dh, fvr, fvg, fvb;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               rgbToLch ((float) sr, (float) sg, (float) sb, sl, sc, sh);
               rgbToLch ((float) dr, (float) dg, (float) db, dl, dc, dh);
               lchToRgb (dl, sc, dh, fvr, fvg, fvb);

               vr = (unsigned int) fvr;
               vg = (unsigned int) fvg;
               vb = (unsigned int) fvb;
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('l'<<24)|('u'<<16)|('m'<<8)|(' '):
      /* Luminosity */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               float sl, sc, sh, dl, dc, dh, fvr, fvg, fvb;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               rgbToLch ((float) sr, (float) sg, (float) sb, sl, sc, sh);
               rgbToLch ((float) dr, (float) dg, (float) db, dl, dc, dh);
               lchToRgb (sl, dc, dh, fvr, fvg, fvb);

               vr = (unsigned int) fvr;
               vg = (unsigned int) fvg;
               vb = (unsigned int) fvb;
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('h'<<24)|('u'<<16)|('e'<<8)|(' '):
      /* Hue */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               float sl, sc, sh, dl, dc, dh, fvr, fvg, fvb;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               rgbToLch ((float) sr, (float) sg, (float) sb, sl, sc, sh);
               rgbToLch ((float) dr, (float) dg, (float) db, dl, dc, dh);
               lchToRgb (dl, dc, sh, fvr, fvg, fvb);

               vr = (unsigned int) fvr;
               vg = (unsigned int) fvg;
               vb = (unsigned int) fvb;
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   case ('c'<<24)|('o'<<16)|('l'<<8)|('r'):
      /* Color */
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            long dx = bDocPlacement ? x : (x - lpLayer->nLeft);
            long dy = bDocPlacement ? y : (y - lpLayer->nTop);

            if (dx >= 0 && dy >= 0 && dx < nImgWidth && dy < nImgHeight) {
               unsigned int nSrcPixel = lpSrcPixels [(y - lpLayer->nTop) * nSrcWidth + (x - lpLayer->nLeft)];
               unsigned int sr = nSrcPixel & 0xff, sg = (nSrcPixel >> 8) & 0xff, sb = (nSrcPixel >> 16) & 0xff, sa = (nSrcPixel >> 24) & 0xff;
               unsigned int *lpDstPixel = &lpImgPixels [dy * nImgWidth + dx];
               unsigned int dr = (*lpDstPixel) & 0xff, dg = ((*lpDstPixel) >> 8) & 0xff, db = ((*lpDstPixel) >> 16) & 0xff;
               float sl, sc, sh, dl, dc, dh, fvr, fvg, fvb;
               unsigned int vr, vg, vb;
               unsigned int nBlend = (sa * nAbsOpacity) / 255;
               unsigned int r, g, b;

               rgbToLch ((float) sr, (float) sg, (float) sb, sl, sc, sh);
               rgbToLch ((float) dr, (float) dg, (float) db, dl, dc, dh);
               lchToRgb (dl, sc, sh, fvr, fvg, fvb);

               vr = (unsigned int) fvr;
               vg = (unsigned int) fvg;
               vb = (unsigned int) fvb;
  
               r = dr + (vr - dr) * nBlend / 255;
               g = dg + (vg - dg) * nBlend / 255;
               b = db + (vb - db) * nBlend / 255;
               
               *lpDstPixel = (PSD_MAX (nBlend, (*lpDstPixel >> 24)) << 24) | (r) | (g << 8) | (b << 16);
            }
         }
      }
      break;

   default:
      /* Unsupported */
      PSD_LOG ("PSD: unsupported blending mode %c%c%c%c for layer '%s'", lpLayer->nBlendingMode >> 24, (lpLayer->nBlendingMode >> 16) & 255, (lpLayer->nBlendingMode >> 8) & 255, lpLayer->nBlendingMode & 255,
             lpLayer->szName);
      break;
   }
}

/** Free resources used by this PSD file */

void PsdDocument::freePsd (void) {
   if (_layer) {
      long i;

      for (i = 0; i < _nLayers; i++) {
         if (_layer[i].channel)
            delete [] _layer[i].channel;

         _layer[i].layerImg.freeImage ();
      }

      delete [] _layer;
      _layer = NULL;
   }
   _nLayers = 0;

   if (_lpPalette) {
      delete [] _lpPalette;
      _lpPalette = NULL;
   }

   if (_auxChannel) {
      free (_auxChannel);
      _auxChannel = NULL;
   }
   _nAuxChannels = 0;
}

/**** PRIVATE ****/

/**
 * Decompress pixel data of the specified layer
 *
 * \param lpImgPixels current pixels of image that this layer will be composited in; used to calculate adjustment layers
 * \param lpLayer layer to decompress
 */

void PsdDocument::decompressLayerPixels (unsigned int *lpImgPixels, PsdLayer *lpLayer) {
   long nWidth, nHeight, nMaskWidth, nMaskHeight, j;
   short i;
   unsigned int *lpPixels;
   unsigned char *lpMask = NULL;
   bool bTransparencyPresent = false;

   if (lpLayer->layerImg.getPixels() && lpLayer->nAdjustmentType == PSD_ADJUSTMENT_NONE) return;

   if (lpLayer->nAdjustmentType != PSD_ADJUSTMENT_NONE) {
      lpLayer->nLeft = 0;
      lpLayer->nTop = 0;
      lpLayer->nRight = _nWidth;
      lpLayer->nBottom = _nHeight;
   }

   nWidth = lpLayer->nRight - lpLayer->nLeft;
   nHeight = lpLayer->nBottom - lpLayer->nTop;
   if (nWidth <= 0 || nHeight <= 0) return;

   if (lpLayer->bMaskEnabled) {
      nMaskWidth = lpLayer->nMaskRight - lpLayer->nMaskLeft;
      nMaskHeight = lpLayer->nMaskBottom - lpLayer->nMaskTop;
   }
   else {
      nMaskWidth = nMaskHeight = 0;
   }

   lpPixels = lpLayer->layerImg.getPixels();
   if (!lpPixels) {
      lpPixels = new unsigned int [nWidth * nHeight];
      memset (lpPixels, 0, nWidth * nHeight * sizeof (unsigned int));
      lpLayer->layerImg.setPixels (nWidth, nHeight, true, lpPixels, false, true);
   }

   switch (lpLayer->nAdjustmentType) {
   case PSD_ADJUSTMENT_EXPOSURE:
      {
         float fMultiplier = powf (2.0f, lpLayer->fExposureValue/2.20f);
         float fInvGamma = 1 / lpLayer->fExposureGamma;

         for (j = 0; j < _nWidth * _nHeight; j++) {
            unsigned int nPixel = lpImgPixels[j];
            float r = (nPixel & 0xff) / 255.0f;
            float g = ((nPixel >> 8) & 0xff) / 255.0f;
            float b = ((nPixel >> 16) & 0xff) / 255.0f;
            unsigned int vr, vg, vb;

            r = powf (r * fMultiplier + lpLayer->fExposureOffset, fInvGamma); if (r < 0) r = 0; if (r > 1) r = 1;
            g = powf (g * fMultiplier + lpLayer->fExposureOffset, fInvGamma); if (g < 0) g = 0; if (g > 1) g = 1;
            b = powf (b * fMultiplier + lpLayer->fExposureOffset, fInvGamma); if (b < 0) b = 0; if (b > 1) b = 1;
            vr = (unsigned int) (r * 255.0f);
            vg = (unsigned int) (g * 255.0f);
            vb = (unsigned int) (b * 255.0f);

            lpPixels[j] = vr | (vg << 8) | (vb << 16);
         }
      }
      break;

   default:
      break;
   }

   for (i = 0; i < lpLayer->nChannels; i++) {
      PsdChannel *lpCurChannel = &lpLayer->channel[i];
      const unsigned char *lpCurData = _lpData + lpCurChannel->nChannelDataOffset;
      const unsigned char *lpEndData = _lpData + _nDataSize;
      long nShift = -1;

      switch (lpCurChannel->nChannelId) {
      case 0:
         /* red or grey */
         if (lpLayer->nAdjustmentType == PSD_ADJUSTMENT_NONE) nShift = 0;
         break;
      case 1:
         /* green */
         if (lpLayer->nAdjustmentType == PSD_ADJUSTMENT_NONE) nShift = 8;
         break;
      case 2:
         /* blue */
         if (lpLayer->nAdjustmentType == PSD_ADJUSTMENT_NONE) nShift = 16;
         break;
      case -1:
         /* transparency */
         if (lpLayer->nAdjustmentType == PSD_ADJUSTMENT_NONE) {
            nShift = 24;
            bTransparencyPresent = true;
         }
         break;
      case -2:
         /* layer mask */
         if (lpLayer->bMaskEnabled && nMaskWidth > 0 && nMaskHeight > 0) {
            if (!lpMask) {
               lpMask = new unsigned char [nMaskWidth * nMaskHeight];
               memset (lpMask, 0, nMaskWidth * nMaskHeight * sizeof (unsigned char));
            }

            nShift = 32;
         }
         break;

      default:
         nShift = -1;
         break;
      }

      if (nShift >= 0) {
         unsigned int nValue;
         long k;
         short nCompressionMethod;

         nCompressionMethod = readPsdShort (lpCurData);
         lpCurData += 2;

         if (nCompressionMethod == 0) {
            /* Uncompressed data */

            if (_nColorMode == 1 && lpCurChannel->nChannelId == 0) {
               /* Greyscale */

               for (j = 0; j < nHeight; j++) {
                  for (k = 0; k < nWidth; k++) {
                     nValue = (unsigned int) (*lpCurData++); 
                     lpPixels[j*nWidth+k] |= (nValue | (nValue << 8) | (nValue << 16));
                  }
               }
            }
            else {
               if (nShift == 32) {
                  /* Layer mask data */
                  for (j = 0; j < nMaskHeight; j++) {
                     for (k = 0; k < nMaskWidth; k++) {
                        lpMask[j*nMaskWidth+k] = *lpCurData++;
                     }
                  }
               }
               else {
                  /* Channel data */
                  for (j = 0; j < nHeight; j++) {
                     for (k = 0; k < nWidth; k++) {
                        nValue = (unsigned int) (*lpCurData++); 
                        lpPixels[j*nWidth+k] |= (nValue << nShift);
                     }
                  }
               }
            }
         }
         else {
            size_t *nLineSize;
            const unsigned char *lpLineData;

            /* RLE compression */
            if (nShift == 32) {
               nLineSize = new size_t [nMaskHeight];
               for (j = 0; j < nMaskHeight; j++) {
                  nLineSize[j] = (size_t) readPsdShort (lpCurData);
                  lpCurData += 2;
               }
            }
            else {
               nLineSize = new size_t [nHeight];
               for (j = 0; j < nHeight; j++) {
                  nLineSize[j] = (size_t) readPsdShort (lpCurData);
                  lpCurData += 2;
               }
            }

            if (_nColorMode == 1 && lpCurChannel->nChannelId == 0) {
               /* Greyscale */

               for (j = 0; j < nHeight; j++) {
                  lpLineData = lpCurData;
                  lpCurData += nLineSize[j];

                  k = 0;
                  while (lpLineData < lpCurData) {
                     long nRunLength = (long) ((char) (*lpLineData++));

                     if (nRunLength < 0) {
                        nValue = (unsigned int) (*lpLineData++); 
                        nRunLength = -nRunLength + 1;

                        while (nRunLength > 0) {
                           lpPixels[j*nWidth+k] |= (nValue | (nValue << 8) | (nValue << 16));
                           k++;
                           nRunLength--;
                        }
                     }
                     else {
                        nRunLength++;

                        while (nRunLength > 0) {
                           nValue = (unsigned int) (*lpLineData++); 
                           lpPixels[j*nWidth+k] |= (nValue | (nValue << 8) | (nValue << 16));
                           k++;
                           nRunLength--;
                        }
                     }

                  }
               }
            }
            else {
               if (nShift == 32) {
                  unsigned char nMaskValue;

                  /* Layer mask data */
                  for (j = 0; j < nMaskHeight; j++) {
                     lpLineData = lpCurData;
                     lpCurData += nLineSize[j];

                     k = 0;
                     while (lpLineData < lpCurData) {
                        long nRunLength = (long) ((char) (*lpLineData++));

                        if (nRunLength < 0) {
                           nMaskValue = (*lpLineData++); 
                           nRunLength = -nRunLength + 1;

                           while (nRunLength > 0) {
                              lpMask[j*nMaskWidth+k] = nMaskValue;
                              k++;
                              nRunLength--;
                           }
                        }
                        else {
                           nRunLength++;

                           while (nRunLength > 0) {
                              nMaskValue = (*lpLineData++); 
                              lpMask[j*nMaskWidth+k] = nMaskValue;
                              k++;
                              nRunLength--;
                           }
                        }
                     }
                  }
               }
               else {
                  /* Channel data */
                  for (j = 0; j < nHeight; j++) {
                     lpLineData = lpCurData;
                     lpCurData += nLineSize[j];

                     k = 0;
                     while (lpLineData < lpCurData) {
                        long nRunLength = (long) ((char) (*lpLineData++));

                        if (nRunLength < 0) {
                           nValue = (unsigned int) (*lpLineData++); 
                           nRunLength = -nRunLength + 1;

                           while (nRunLength > 0) {
                              lpPixels[j*nWidth+k] |= (nValue << nShift);
                              k++;
                              nRunLength--;
                           }
                        }
                        else {
                           nRunLength++;

                           while (nRunLength > 0) {
                              nValue = (unsigned int) (*lpLineData++); 
                              lpPixels[j*nWidth+k] |= (nValue << nShift);
                              k++;
                              nRunLength--;
                           }
                        }

                     }
                  }
               }
            }

            delete [] nLineSize;
         }
      }      
   }

   if (_nColorMode == 3 && !bTransparencyPresent) {
      long x;

      /* Transparency channel not present; set to no transparency */
      for (x = 0; x < nWidth * nHeight; x++)
         lpPixels[x] |= 0xff000000;
   }

   if (lpMask) {
      long x, y;

      /* Apply layer mask */
      for (y = lpLayer->nMaskTop; y < lpLayer->nMaskBottom; y++) {
         for (x = lpLayer->nMaskLeft; x < lpLayer->nMaskRight; x++) {
            long u = x - lpLayer->nLeft;
            long v = y - lpLayer->nTop;

            if (u >= 0 && v >= 0 && u < nWidth && v < nHeight) {
               unsigned int nPixel = lpPixels[v * nWidth + u];
               unsigned int nMask = (unsigned int) lpMask[(y - lpLayer->nMaskTop) * nMaskWidth + (x - lpLayer->nMaskLeft)];

               lpPixels[v * nWidth + u] = (nPixel & 0x00ffffff) | (((nPixel >> 24) * nMask / 255) << 24);
            }
         }
      }

      for (y = lpLayer->nTop; y < lpLayer->nMaskTop; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            unsigned int nPixel = lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)];
            lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)] = (nPixel & 0x00ffffff) | (((nPixel >> 24) * lpLayer->nMaskDefaultColor / 255) << 24);
         }
      }
      for (y = lpLayer->nMaskBottom; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nRight; x++) {
            unsigned int nPixel = lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)];
            lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)] = (nPixel & 0x00ffffff) | (((nPixel >> 24) * lpLayer->nMaskDefaultColor / 255) << 24);
         }
      }
      for (y = lpLayer->nTop; y < lpLayer->nBottom; y++) {
         for (x = lpLayer->nLeft; x < lpLayer->nMaskLeft; x++) {
            unsigned int nPixel = lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)];
            lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)] = (nPixel & 0x00ffffff) | (((nPixel >> 24) * lpLayer->nMaskDefaultColor / 255) << 24);
         }
         for (x = lpLayer->nMaskRight; x < lpLayer->nRight; x++) {
            unsigned int nPixel = lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)];
            lpPixels[(y - lpLayer->nTop) * nWidth + (x - lpLayer->nLeft)] = (nPixel & 0x00ffffff) | (((nPixel >> 24) * lpLayer->nMaskDefaultColor / 255) << 24);
         }
      }

      delete [] lpMask;
   }
}

/**
 * Decompress background pixel data of the specified layer
 *
 * \param nOffset starting offset of image data in file
 * \param lpImage image to decompress into
 */

void PsdDocument::decompressBackgroundPixels (size_t nOffset, PsdLayerImage *lpImage) {
   const unsigned char *lpCurData = _lpData + nOffset;
   const unsigned char *lpEndData = _lpData + _nDataSize;
   short nCompressionMethod;
   unsigned int *lpPixels;
   long i, j, k;
   
   nCompressionMethod = readPsdShort (lpCurData);
   lpCurData += 2;

   lpPixels = new unsigned int [_nWidth * _nHeight];
   lpImage->setPixels (_nWidth, _nHeight, true, lpPixels, false, true);

   if (nCompressionMethod == 0) {
      /* Uncompressed data */

      if (_nColorMode == 1) {
         unsigned int nValue;

         /* Greyscale */
         for (j = 0; j < _nHeight; j++) {
            for (k = 0; k < _nWidth; k++) {
               nValue = (unsigned int) (*lpCurData++); 
               lpPixels[j*_nWidth+k] = nValue | (nValue << 8) | (nValue << 16) | 0xff000000;
            }
         }
      }
      else if (_nColorMode == 3) {
         /* RGB */

         for (i = 0; i < _nWidth * _nHeight; i++)
            lpPixels[i] = 0xff000000;

         for (i = 0; i < _nChannels; i++) {
            long nShift = i * 8;
            unsigned int nValue;

            for (j = 0; j < _nHeight; j++) {
               for (k = 0; k < _nWidth; k++) {
                  nValue = (unsigned int) (*lpCurData++); 
                  lpPixels[j*_nWidth+k] |= (nValue << nShift);
               }
            }
         }
      }
   }
   else {
      size_t *nLineSize;
      const unsigned char *lpLineData;

      /* RLE compression */
      nLineSize = new size_t [_nChannels*_nHeight];
      for (j = 0; j < _nChannels*_nHeight; j++) {
         nLineSize[j] = (size_t) readPsdShort (lpCurData);
         lpCurData += 2;
      }

      if (_nColorMode == 1) {
         unsigned int nValue;

         /* Greyscale */
         for (j = 0; j < _nHeight; j++) {
            lpLineData = lpCurData;
            lpCurData += nLineSize[j];

            k = 0;
            while (lpLineData < lpCurData) {
               long nRunLength = (long) ((char) (*lpLineData++));

               if (nRunLength < 0) {
                  nValue = (unsigned int) (*lpLineData++); 
                  nRunLength = -nRunLength + 1;

                  while (nRunLength > 0) {
                     lpPixels[j*_nWidth+k] = nValue | (nValue << 8) | (nValue << 16) | 0xff000000;
                     k++;
                     nRunLength--;
                  }
               }
               else {
                  nRunLength++;

                  while (nRunLength > 0) {
                     nValue = (unsigned int) (*lpLineData++); 
                     lpPixels[j*_nWidth+k] = nValue | (nValue << 8) | (nValue << 16) | 0xff000000;
                     k++;
                     nRunLength--;
                  }
               }
            }
         }
      }
      if (_nColorMode == 2) {
         unsigned int nValue;

         /* Indexed color */
         for (j = 0; j < _nHeight; j++) {
            lpLineData = lpCurData;
            lpCurData += nLineSize[j];

            k = 0;
            while (lpLineData < lpCurData) {
               long nRunLength = (long) ((char) (*lpLineData++));

               if (nRunLength < 0) {
                  nValue = (unsigned int) (*lpLineData++);
                  if (nValue < _nPaletteEntries)
                     nValue = _lpPalette[nValue];
                  else
                     nValue = 0;
                  nRunLength = -nRunLength + 1;

                  while (nRunLength > 0) {
                     lpPixels[j*_nWidth+k] = nValue | 0xff000000;
                     k++;
                     nRunLength--;
                  }
               }
               else {
                  nRunLength++;

                  while (nRunLength > 0) {
                     nValue = (unsigned int) (*lpLineData++);
                     if (nValue < _nPaletteEntries)
                        nValue = _lpPalette[nValue];
                     else
                        nValue = 0;
                     lpPixels[j*_nWidth+k] = nValue | 0xff000000;
                     k++;
                     nRunLength--;
                  }
               }
            }
         }
      }
      else if (_nColorMode == 3) {
         /* RGB */

         for (i = 0; i < _nWidth * _nHeight; i++)
            lpPixels[i] = 0xff000000;

         for (i = 0; i < _nChannels; i++) {
            long nShift = i * 8;
            unsigned int nValue;

            for (j = 0; j < _nHeight; j++) {
               lpLineData = lpCurData;
               lpCurData += nLineSize[j+i*_nHeight];

               k = 0;
               while (lpLineData < lpCurData) {
                  long nRunLength = (long) ((char) (*lpLineData++));

                  if (nRunLength < 0) {
                     nValue = (unsigned int) (*lpLineData++); 
                     nRunLength = -nRunLength + 1;

                     while (nRunLength > 0) {
                        lpPixels[j*_nWidth+k] |= (nValue << nShift);
                        k++;
                        nRunLength--;
                     }
                  }
                  else {
                     nRunLength++;

                     while (nRunLength > 0) {
                        nValue = (unsigned int) (*lpLineData++); 
                        lpPixels[j*_nWidth+k] |= (nValue << nShift);
                        k++;
                        nRunLength--;
                     }
                  }
               }
            }
         }
      }

      delete [] nLineSize;
   }
}

/**
 * Read 16-bit big endian word
 *
 * \param lpBytes location
 *
 * \return 16-bit word in native endianness
 */

short PsdDocument::readPsdShort (const unsigned char *lpBytes) {
   return (short) ((((unsigned short) lpBytes[0]) << 8) | ((unsigned long) lpBytes[1]));
}

/**
 * Read 32-bit big endian word
 *
 * \param lpBytes location
 *
 * \return 32-bit word in native endianness
 */

long PsdDocument::readPsdLong (const unsigned char *lpBytes) {
   return (long) ((((unsigned long) lpBytes[0]) << 24) | (((unsigned long) lpBytes[1]) << 16) | (((unsigned long) lpBytes[2]) << 8) | ((unsigned long) lpBytes[3]));
}

/**
 * Convert rgb value to hsl value
 *
 * \param r red component
 * \param g green component
 * \param b blue component
 * \param luma returned lightness
 * \param chroma returned chroma
 * \param hue returned hue
 */

void PsdDocument::rgbToLch (float r, float g, float b, float &luma, float &chroma, float &hue) {
   float min, max, delta;

   r /= 255.0f;
   g /= 255.0f;
   b /= 255.0f;

   min = r;
   max = r;

   if(g < min) min = g;
   else max = g;

   if (b < min) min = b;
   else if (b > max) max = b;

   delta = max - min;
   chroma = delta;
   luma   = 0.299f*r + 0.587f*g + 0.114f*b;

   if (delta < 0 || delta > 0) {
      float inverseDelta = (1.0f/6) / delta, h;
      if(r == max) h = (g-b)*inverseDelta;
      else if(g == max) h = (b-r)*inverseDelta + 1.0f/3;
      else h = (r-g)*inverseDelta + 2.0f/3;
      if(h >= 1) h -= 1;
      else if(h < 0) h += 1;

      hue = h;
   }
   else
      hue = 0;
}

/**
 * Convert hsl value to rgb value
 *
 * \param luma lightness
 * \param chroma chroma
 * \param hue hue
 * \param r returned red component
 * \param g returned green component
 * \param b returned blue component
 */

void PsdDocument::lchToRgb (float luma, float chroma, float hue, float &r, float &g, float &b) {
    if (chroma < 0 || chroma > 1 || luma < 0 || luma > 1) {
       r = g = b = 0;
    }
    else {
       if(hue >= 1) {
         hue -= (int)hue; // e.g. 2.25 -> 2.25 - 2 == 0.25, 2 -> 2 - 2 == 0
       }
       else if(hue < 0)
       {
         hue -= (int)hue - 1; // e.g. -2.25 -> -2.25 - -3 == -2.25 + 3 == 0.75, BUT -2 -> -2 - -3 == -2 + 3 == 1
         if(hue == 1) hue = 0; // if the hue was already an integer, then it'll have become equal to 1, so make it zero
       }

       float hueFace = hue*6;
       int hueFaceInt = (int)hueFace, term = (hueFaceInt+1) & ~1;
       float hff = (hueFaceInt&1) == 0 ? hueFace - term : term - hueFace;
       bool clipped = false;

       recalculate:
       float x = chroma * hff;
       switch(hueFaceInt) {
         case 0: r = chroma; g = x; b = 0; break;
         case 1: r = x; g = chroma; b = 0; break;
         case 2: r = 0; g = chroma; b = x; break;
         case 3: r = 0; g = x; b = chroma; break;
         case 4: r = x; g = 0; b = chroma; break;
         default: r = chroma; g = 0; b = x; break;
       }

       x  = luma - 0.299f*r - 0.587f*g - 0.114f*b;
       r += x;
       g += x;
       b += x;

       if (!clipped) {
         float min = r, max = r;
         if(g < min) min = g;
         else max = g;
         if(b < min) min = b;
         else if(b > max) max = b;

         if(max > 1) chroma *= (1 - max) / (max - luma) + 1;
         else if(min < 0) chroma *= -min / (min - luma) + 1;

         clipped = true;
         goto recalculate;
       } 
    }

    r = floorf (r*255+0.5f); if (r < 0) r = 0; if (r > 255) r = 255;
    g = floorf (g*255+0.5f); if (g < 0) g = 0; if (g > 255) g = 255;
    b = floorf (b*255+0.5f); if (b < 0) b = 0; if (b > 255) b = 255;
}
